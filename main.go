package main

import (
	"bitbucket.org/lexty/docker-nginx-auto-proxy/docker"
	"bitbucket.org/lexty/docker-nginx-auto-proxy/nginx"
	"bitbucket.org/lexty/docker-nginx-auto-proxy/resources"
	"bitbucket.org/lexty/docker-nginx-auto-proxy/version"
	"fmt"
	"gopkg.in/urfave/cli.v1"
	"log"
	"net"
	"os"
	"text/template"
)

//var configFile string
var cfg struct {
	Network  string
	ConfPath string
}

func main() {
	app := cli.NewApp()
	app.Name = "Docker Nginx Auto Reverse Proxy"
	app.Version = version.GetVersion()
	app.Compiled = version.GetBuildTime()
	app.Usage = "Autoconfigured reverse proxy for http services in docker dontainers"
	app.Authors = []cli.Author{
		cli.Author{
			Name:  "Alexandr Medvedev",
			Email: "alexandr.mdr@gmail.com",
		},
	}

	flags := []cli.Flag{
		//cli.StringFlag{
		//	Name:        "config, c",
		//	Value:       os.Getenv("HOME") + string(os.PathSeparator) + ".docker-nginx-auto-proxy.yaml",
		//	Usage:       "Load configuration from `FILE`",
		//	Destination: &configFile,
		//	EnvVar:      "DOCKER_NGINX_AUTO_PROXY_CONFIG",
		//},
		cli.StringFlag{
			Name:        "networks, n",
			Value:       "eth0",
			Usage:       "Networks (comma separated) for find servers",
			Destination: &cfg.Network,
			EnvVar:      "DOCKER_NGINX_AUTO_PROXY_NETWORK",
		},
		cli.StringFlag{
			Name:        "path, p",
			Value:       nginx.DEFAULT_CONF_PATH,
			Usage:       "The path to virtual hosts configureation directory",
			Destination: &cfg.ConfPath,
			EnvVar:      "DOCKER_NGINX_AUTO_PROXY_PATH",
		},
	}

	app.Action = func(c *cli.Context) error {
		doc := docker.NewClient()
		cs, err := doc.Inspect("bridge")
		if err != nil {
			log.Fatalln(err)
		}

		fmt.Printf("%v\n\n", cs[0].HostName)

		tmpl, err := template.New("test").Parse(resources.Vhostconf)
		//tmplSsl, err := template.New("test").Parse(resources.Vhostsslconf)

		for _, container := range cs {
			out, err := os.Create(cfg.ConfPath + "/" + container.Name + ".cnt.conf")
			if err != nil {
				log.Fatalln(err)
			}
			err = tmpl.Execute(out, container)
			if err != nil {
				panic(err)
			}
		}

		nginx.ReloadServer()

		return nil
	}

	app.Flags = flags
	app.Run(os.Args)
}

func LocalAddresses() {
	ifaces, err := net.Interfaces()
	if err != nil {
		log.Print(fmt.Errorf("localAddresses: %v\n", err.Error()))
		return
	}
	for _, i := range ifaces {
		addrs, err := i.Addrs()
		if err != nil {
			log.Print(fmt.Errorf("localAddresses: %v\n", err.Error()))
			continue
		}
		for _, a := range addrs {
			log.Printf("%v %v\n", i.Name, a)
		}
	}
}

//func Print(c []docker.Container) {
//	for
//}

//go:generate go run generate/tpl.go
