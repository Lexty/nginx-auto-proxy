package nginx

import "os/exec"

const DEFAULT_CONF_PATH = "/etc/nginx/conf.d"

func ReloadServer() {
	exec.Command("/bin/sh", "-c", "nginx reload").Output()
}
