package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"os"
	"strings"
)

const (
	PACKAGE_NAME = "resources"
	PATH         = "./resources"
)

func main() {
	strings.Replace(PATH, "/", string(os.PathSeparator), -1)
	fs, _ := ioutil.ReadDir(getPath(PATH))
	for _, f := range fs {
		if strings.HasSuffix(f.Name(), ".go") {
			continue
		}

		name := resToGo(f.Name())

		out, err := os.Create(getPath(PATH + "/" + name + ".go"))
		if err != nil {
			log.Fatalln(err)
		}
		out.Write([]byte(fmt.Sprintf("package %s \n\nconst (\n", PACKAGE_NAME)))

		out.Write([]byte(strings.Title(name) + " = `"))
		f, err := os.Open(getPath(PATH + "/" + f.Name()))
		if err != nil {
			log.Fatalln(err)
		}
		io.Copy(out, f)
		out.Write([]byte("`\n)\n"))
	}
}

func getPath(path string) string {
	return strings.Replace(path, "/", string(os.PathSeparator), -1)
}

func resToGo(res string) string {
	nameParts := strings.Split(res, ".")
	if len(nameParts) > 0 {
		nameParts = nameParts[:len(nameParts)-1]
	}

	return strings.Join(nameParts, "")
}
