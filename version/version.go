package version

import (
	"strings"
	"time"
)

var num string
var buildTime string
var buildGitHash string

func GetVersion() string {
	return num
}

func GetBuildTime() time.Time {
	t, err := time.Parse("2006-01-02T15:04:05 MST", strings.Replace(buildTime, "___", " ", -1))
	if err != nil {
		panic(err)
	}
	return t
}

func GetBuildGitHash() string {
	return buildGitHash
}
