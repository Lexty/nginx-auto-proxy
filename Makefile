BIN := dnap
VERSION := 0.0.1
BUILD_DATE := $(shell date -u "+%Y-%m-%dT%H:%M:%S___UTC")
BUILD_HASH := $(shell git log -1 --pretty=format:"%H")
BUILD_USERNAME := $(shell git config user.name)
BUILD_USERMAIL := $(shell git config user.email)

PACKAGE_PATH := bitbucket.org/lexty/docker-nginx-auto-proxy

all: build

build: generate fmt
	go build -ldflags '-X $(PACKAGE_PATH)/version.num=$(VERSION) -X $(PACKAGE_PATH)/version.buildTime=$(BUILD_DATE) -X $(PACKAGE_PATH)/version.buildGitHash=$(BUILD_HASH)' -o build/$(BIN)
#	go build -ldflags '-X $(PACKAGE_PATH)/version.Version="$(VERSION)" -X $(PACKAGE_PATH)/version.BuildTime="$(BUILD_DATE)" -X $(PACKAGE_PATH)/version.BuildGitHash="$(BUILD_HASH)" -X $(PACKAGE_PATH)/version.BuildAuthorName="$(BUILD_USERNAME)" -X $(PACKAGE_PATH)/version.BuildAuthorEMail="$(BUILD_USERMAIL)"' -o build/$(BIN)

run: build
	build/$(BIN)

generate:
	go generate

fmt:
	go fmt ./...

clean:
	rm resources/*.go
	rm -rf build

.PHONY: generate fmt clean
