package docker

import (
	"encoding/json"
	"fmt"
	"net"
	"os/exec"
	"strconv"
)

type Client struct {
	Containers Containers
}

const DEFAULT_HTTP_PORT = 80
const DEFAULT_HTTPS_PORT = 443

type Container struct {
	ID        string
	Name      string
	IP        net.IP
	HostName  string
	HTTPPort  int
	HTTPSPort int
}

func (c Container) GetHTTPPort() int {
	if c.HTTPPort == 0 {
		return DEFAULT_HTTP_PORT
	}
	return c.HTTPPort
}

func (c Container) GetServerName() string {
	if c.HostName == "" {
		return c.Name
	}
	return c.HostName
}

type Containers []Container

type rawNetInspect []struct {
	Name       string
	Containers map[string]struct {
		Name        string
		IPv4Address string
		IPv6Address string
	}
}

type rawContInspect []struct {
	Config struct {
		Labels map[string]string
	}
}

func (c *Client) Inspect(network string) ([]Container, error) {
	out, _ := exec.Command("/bin/sh", "-c", "docker network inspect "+network).Output()
	resp := rawNetInspect{}
	err := json.Unmarshal(out, &resp)
	if err != nil {
		return nil, err
	}

	containers := []Container{}
	var container Container

	for _, netw := range resp {
		if netw.Name != network {
			continue
		} else {

		}
		for contID, contData := range netw.Containers {
			IP, _, _ := net.ParseCIDR(contData.IPv4Address)
			container = Container{
				ID:   contID,
				Name: contData.Name,
				IP:   IP,
			}
			err := contInspect(&container)
			if err != nil {
				return nil, err
			}
			containers = append(containers, container)
		}
	}

	return containers, nil
}

func NewClient() *Client {
	return &Client{}
}

func contInspect(c *Container) error {
	out, _ := exec.Command("/bin/sh", "-c", "docker inspect "+c.Name).Output()
	resp := rawContInspect{}
	err := json.Unmarshal(out, &resp)
	if err != nil {
		return err
	}

	fmt.Printf("%v\n\n", resp)

	c.HostName = resp[0].Config.Labels["HostName"]
	c.HTTPPort, _ = strconv.Atoi(resp[0].Config.Labels["HTTPPort"])
	c.HTTPSPort, _ = strconv.Atoi(resp[0].Config.Labels["HTTPPort"])
	return nil
}

//  [
//  {
//  "Name": "bridge",
//  "Id": "9d5256a26c3ca847103cd330e3f73b0e46c82e955db46a69a2faa4745b1446df",
//  "Scope": "local",
//  "Driver": "bridge",
//  "EnableIPv6": false,
//  "IPAM": {
//  "Driver": "default",
//  "Options": null,
//  "Config": [
//  {
//  "Subnet": "172.17.0.0/16",
//  "Gateway": "172.17.0.1"
//  }
//  ]
//  },
//  "Internal": false,
//  "Containers": {
//  "6abc753c56cbee5967e55ad55b62d6771216f1e8085c5164d5e477dbc20ae268": {
//  "Name": "container1",
//  "EndpointID": "78f547dfa2efae2f922cd0b2ebc64f6790bcfa041b1f2e755c0b29677e5dfaf4",
//  "MacAddress": "02:42:ac:11:00:03",
//  "IPv4Address": "172.17.0.3/16",
//  "IPv6Address": ""
//  },
//  "e86c6cc71bcfb753df7e1a32845e622c5062ed9b774a117ac19ccf11414ea258": {
//  "Name": "container2",
//  "EndpointID": "997658cab6d2586a6eab4092ff9edd09efc74be95fc3633a9be80e5d7f00ada1",
//  "MacAddress": "02:42:ac:11:00:02",
//  "IPv4Address": "172.17.0.2/16",
//  "IPv6Address": ""
//  }
//  },
//  "Options": {
//  "com.docker.network.bridge.default_bridge": "true",
//  "com.docker.network.bridge.enable_icc": "true",
//  "com.docker.network.bridge.enable_ip_masquerade": "true",
//  "com.docker.network.bridge.host_binding_ipv4": "0.0.0.0",
//  "com.docker.network.bridge.name": "docker0",
//  "com.docker.network.driver.mtu": "1500"
//  },
//  "Labels": {}
//  }
//  ]
