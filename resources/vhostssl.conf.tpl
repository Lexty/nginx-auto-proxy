server {
	listen 80;
	server_name {{.HostName}};
	return 301 https://$host$request_uri;
}

server {
	listen 443 ssl;

	ssl_certificate /etc/letsencrypt/live/git.eldios.ru/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/git.eldios.ru/privkey.pem;

    server_name {{.HostName}};
    location / {
        proxy_pass http://{{.IP}}:{{.HTTPPort}};
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
    }
}